package fr.iutfbleau.projetIHM2021FI2.Vue;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;

import fr.iutfbleau.projetIHM2021FI2.Controleur.*;

import java.awt.*;
import java.util.*;

public class BottomContainer extends JPanel {

	private JTable table;
	private JPanel recap;
	//private JTitledBorder title;

	private Set<String> rowContent;
	private Iterator<String> preresItr;
	private HashMap<String, String> infos;
	private JLabel ref;
	private JLabel date;
	private JLabel jours;
	private JLabel typeChambre;
	private JLabel id;
	private JLabel prenomClient;
	private JLabel nomClient;

	private JButton quitter;
	private JButton reserver;

	private DefaultTableModel model;

	public BottomContainer() {
		this.recap = new JPanel();
		this.infos = new HashMap<>();
		this.prenomClient = new JLabel();
		this.nomClient = new JLabel();
		this.quitter = new JButton("Quitter");
		this.reserver = new JButton("Réserver");

		this.setLayout(new GridBagLayout());
		this.setPreferredSize(new Dimension(350, 320));

		ref = new JLabel("Référence : ");
		date = new JLabel("Date : ");
		jours = new JLabel("Jours : ");
		typeChambre = new JLabel("Type Chambre : ");
		id = new JLabel("ID : ");
		prenomClient = new JLabel("Prénom : ");
		nomClient = new JLabel("Nom : ");

		// Panneau récapitulatif des informations de la chambre
		recap.setLayout(new GridLayout(7, 1));
		recap.setPreferredSize(new Dimension(250, 300));
		//recap.setBorder(new Border());
		recap.add(ref);
		recap.add(date);
		recap.add(jours);
		recap.add(typeChambre);
		recap.add(id);
		recap.add(prenomClient);
		recap.add(nomClient);

		// Mise en forme du tableau
		// String[] columnNames = {"Reference",
		// 						"Date",
		// 						"Jours",
		// 						"Type Chambre",
		// 						"ID",
		// 						"Prenom",
		// 						"Nom"};

		// Object[][] data = {
		// 	{"4751-3708-LRMF", "2018-01-05", 1, "UNLD", 1, "Marine", "Carpentier"},
		// 	{"2436-3909-NXLL", "2018-01-07", 1, "UNLS", 1, "Marine", "Carpentier"}
		// };

		table = new JTable(model) {
			@Override
			public boolean editCellAt(int row, int column, java.util.EventObject e) {
				return false;
			}
		};

		// Mise en forme du tableau
		model = (DefaultTableModel) table.getModel();
		model.addColumn("Reference");
		model.addColumn("Date");
		model.addColumn("Jours");
		model.addColumn("Type Chambre");
		model.addColumn("ID");
		model.addColumn("Prenom");
		model.addColumn("Nom");
		
		//getFocusedComponent
		table.getTableHeader().setReorderingAllowed(false);
		table.setPreferredSize(new Dimension(700, 250));
		table.getTableHeader().setPreferredSize(new Dimension(700, 40));
		//table.setCellSelectionEnabled(false);
		//table.setColumnSelectionAllowed(false);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		//table.setDragEnabled(false);
		// Ajout des listeners
		table.getSelectionModel().addListSelectionListener(new RowListener(table, this));
		quitter.addActionListener(new QuitListener());
		reserver.addActionListener(new ReservListener());

		// Reste de la fenêtre
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;      // la plage de cellules commence à la première colonne
		gbc.gridy = 0;      // la plage de cellules commence à la deuxième ligne
		gbc.gridwidth = 2;  // la plage de cellules englobe deux colonnes
		gbc.gridheight = 1; // la plage de cellules englobe une seule ligne
		gbc.fill = GridBagConstraints.HORIZONTAL;     // n'occupe pas tout l'espace de la plage
		gbc.anchor = GridBagConstraints.NORTHWEST; // se place au centre de la plage
		gbc.weightx = 1.0;  // souhaite plus de largeur si possible
		gbc.weighty = 1.0;  // n'a pas besoin de hauteur supplémentaire
		gbc.insets = new Insets(5, 5, 0, 5);    // laisse 5 pixels de vide autour du composant
		this.add(table.getTableHeader(), gbc);

		gbc.gridx = 0;      // la plage de cellules commence à la première colonne
		gbc.gridy = 1;      // la plage de cellules commence à la deuxième ligne
		gbc.gridwidth = 2;  // la plage de cellules englobe deux colonnes
		gbc.gridheight = 1; // la plage de cellules englobe une seule ligne
		gbc.fill = GridBagConstraints.BOTH;     // n'occupe pas tout l'espace de la plage
		gbc.anchor = GridBagConstraints.CENTER; // se place au centre de la plage
		gbc.weightx = 1.0;  // souhaite plus de largeur si possible
		gbc.weighty = 1.0;  // n'a pas besoin de hauteur supplémentaire
		gbc.insets = new Insets(0, 5, 5, 5);    // laisse 5 pixels de vide autour du composant
		this.add(table, gbc);

		gbc.gridx = 2;      // la plage de cellules commence à la première colonne
		gbc.gridy = 0;      // la plage de cellules commence à la deuxième ligne
		gbc.gridwidth = 1;  // la plage de cellules englobe deux colonnes
		gbc.gridheight = 3; // la plage de cellules englobe une seule ligne
		gbc.fill = GridBagConstraints.BOTH;     // n'occupe pas tout l'espace de la plage
		gbc.anchor = GridBagConstraints.CENTER; // se place au centre de la plage
		gbc.weightx = 1.0;  // souhaite plus de largeur si possible
		gbc.weighty = 1.0;  // n'a pas besoin de hauteur supplémentaire
		gbc.insets = new Insets(5, 5, 5, 5);    // laisse 5 pixels de vide autour du composant
		this.add(recap, gbc);

		gbc.gridx = 0;      // la plage de cellules commence à la première colonne
		gbc.gridy = 2;      // la plage de cellules commence à la deuxième ligne
		gbc.gridwidth = 1;  // la plage de cellules englobe deux colonnes
		gbc.gridheight = 1; // la plage de cellules englobe une seule ligne
		gbc.fill = GridBagConstraints.NONE;     // n'occupe pas tout l'espace de la plage
		gbc.anchor = GridBagConstraints.CENTER; // se place au centre de la plage
		gbc.weightx = 1.0;  // souhaite plus de largeur si possible
		gbc.weighty = 1.0;  // n'a pas besoin de hauteur supplémentaire
		gbc.insets = new Insets(5, 5, 5, 5);    // laisse 5 pixels de vide autour du composant
		this.add(quitter, gbc);

		gbc.gridx = 1;      // la plage de cellules commence à la première colonne
		gbc.gridy = 2;      // la plage de cellules commence à la deuxième ligne
		gbc.gridwidth = 1;  // la plage de cellules englobe deux colonnes
		gbc.gridheight = 1; // la plage de cellules englobe une seule ligne
		gbc.fill = GridBagConstraints.NONE;     // n'occupe pas tout l'espace de la plage
		gbc.anchor = GridBagConstraints.CENTER; // se place au centre de la plage
		gbc.weightx = 1.0;  // souhaite plus de largeur si possible
		gbc.weighty = 1.0;  // n'a pas besoin de hauteur supplémentaire
		gbc.insets = new Insets(5, 5, 5, 5);    // laisse 5 pixels de vide autour du composant
		this.add(reserver, gbc);
	}

	public DefaultTableModel getModel() {
		return model;
	}

	public void setRowContent(Set<String> set) {
		this.rowContent = set;

		preresItr = rowContent.iterator();
	}

	public void addNewDataAndRefresh() {
		// TODO
		/* Parcourir le set et former la HashMap au fur et à mesure
		infos.put("nomColonne", *le set*) */
		
		infos.put("Référence", preresItr.next());
		infos.put("Date", preresItr.next());
		infos.put("Jours", preresItr.next());
		infos.put("Type Chambre", preresItr.next());
		infos.put("ID", preresItr.next());
		infos.put("Prénom", preresItr.next());
		infos.put("Nom", preresItr.next());

		ref.setText("Référence : " + infos.get("Référence"));
		date.setText("Date : " + infos.get("Date"));
		jours.setText("Jours : " + infos.get("Jours"));
		typeChambre.setText("Type Chambre : " + infos.get("Type Chambre"));
		id.setText("ID : " + infos.get("ID"));
		prenomClient.setText("Prénom : " + infos.get("Prénom"));
		nomClient.setText("Nom : " + infos.get("Nom"));

		recap.repaint();
	}
}