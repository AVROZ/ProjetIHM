package fr.iutfbleau.projetIHM2021FI2.Vue;

import javax.swing.*;

import fr.iutfbleau.projetIHM2021FI2.Controleur.*;

import java.awt.*;

public class Fenetre {
    private JFrame fenetre;
    private JTextField prenom;
    private JTextField nom;
    private JTextField reference;
    private JButton searchButton;

    private BottomContainer botCont;

    public Fenetre() {
        this.fenetre = new JFrame();
        this.prenom = new JTextField("Prénom");
        this.nom = new JTextField("Nom");
        this.reference = new JTextField("Référence");
        this.searchButton = new JButton("Chercher");
        this.botCont = new BottomContainer();

        fenetre.setSize(1000, 500);
        fenetre.setLocation(100, 100);
        fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fenetre.setLayout(new GridBagLayout());
        fenetre.setResizable(false);

        searchButton.setPreferredSize(new Dimension(100, 40));

        // Ajout des listeners
        prenom.addFocusListener(new PrenomListener());
        nom.addFocusListener(new NomListener());
        reference.addFocusListener(new RefListener());
        searchButton.addActionListener(new SearchListener(prenom, nom, reference, botCont.getModel()));

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;      // la plage de cellules commence à la première colonne
        gbc.gridy = 0;      // la plage de cellules commence à la deuxième ligne
        gbc.gridwidth = 1;  // la plage de cellules englobe deux colonnes
        gbc.gridheight = 1; // la plage de cellules englobe une seule ligne
        gbc.fill = GridBagConstraints.BOTH;     // n'occupe pas tout l'espace de la plage
        gbc.anchor = GridBagConstraints.CENTER; // se place au centre de la plage
        gbc.weightx = 1.0;  // souhaite plus de largeur si possible
        gbc.weighty = 1.0;  // n'a pas besoin de hauteur supplémentaire
        gbc.insets = new Insets(5, 5, 5, 5);    // laisse 5 pixels de vide autour du composant
        fenetre.add(prenom, gbc);
        
        gbc.gridx = 1;      // la plage de cellules commence à la première colonne
        gbc.gridy = 0;      // la plage de cellules commence à la deuxième ligne
        gbc.gridwidth = 1;  // la plage de cellules englobe deux colonnes
        gbc.gridheight = 1; // la plage de cellules englobe une seule ligne
        gbc.fill = GridBagConstraints.BOTH;     // n'occupe pas tout l'espace de la plage
        gbc.anchor = GridBagConstraints.CENTER; // se place au centre de la plage
        gbc.weightx = 1.0;  // souhaite plus de largeur si possible
        gbc.weighty = 1.0;  // n'a pas besoin de hauteur supplémentaire
        gbc.insets = new Insets(5, 5, 5, 5);    // laisse 5 pixels de vide autour du composant
        fenetre.add(nom, gbc);

        gbc.gridx = 0;      // la plage de cellules commence à la première colonne
        gbc.gridy = 1;      // la plage de cellules commence à la deuxième ligne
        gbc.gridwidth = 2;  // la plage de cellules englobe deux colonnes
        gbc.gridheight = 1; // la plage de cellules englobe une seule ligne
        gbc.fill = GridBagConstraints.BOTH;     // n'occupe pas tout l'espace de la plage
        gbc.anchor = GridBagConstraints.CENTER; // se place au centre de la plage
        gbc.weightx = 1.0;  // souhaite plus de largeur si possible
        gbc.weighty = 1.0;  // n'a pas besoin de hauteur supplémentaire
        gbc.insets = new Insets(5, 5, 5, 5);    // laisse 5 pixels de vide autour du composant
        fenetre.add(reference, gbc);

        gbc.gridx = 2;      // la plage de cellules commence à la première colonne
        gbc.gridy = 0;      // la plage de cellules commence à la deuxième ligne
        gbc.gridwidth = 1;  // la plage de cellules englobe deux colonnes
        gbc.gridheight = 2; // la plage de cellules englobe une seule ligne
        gbc.fill = GridBagConstraints.HORIZONTAL;     // n'occupe pas tout l'espace de la plage
        gbc.anchor = GridBagConstraints.CENTER; // se place au centre de la plage
        gbc.weightx = 1.0;  // souhaite plus de largeur si possible
        gbc.weighty = 0.8;  // n'a pas besoin de hauteur supplémentaire
        gbc.insets = new Insets(5, 5, 5, 5);    // laisse 5 pixels de vide autour du composant
        fenetre.add(searchButton, gbc);

        gbc.gridx = 0;      // la plage de cellules commence à la première colonne
        gbc.gridy = 2;      // la plage de cellules commence à la deuxième ligne
        gbc.gridwidth = 3;  // la plage de cellules englobe deux colonnes
        gbc.gridheight = 1; // la plage de cellules englobe une seule ligne
        gbc.fill = GridBagConstraints.BOTH;     // n'occupe pas tout l'espace de la plage
        gbc.anchor = GridBagConstraints.CENTER; // se place au centre de la plage
        gbc.weightx = 1.0;  // souhaite plus de largeur si possible
        gbc.weighty = 1.0;  // n'a pas besoin de hauteur supplémentaire
        gbc.insets = new Insets(5, 5, 5, 5);    // laisse 5 pixels de vide autour du composant
        fenetre.add(botCont, gbc);

        fenetre.setVisible(true);
    }
}