package fr.iutfbleau.projetIHM2021FI2.Controleur;

import java.awt.event.*;
import java.time.LocalDate;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import fr.iutfbleau.projetIHM2021FI2.API.Prereservation;
import fr.iutfbleau.projetIHM2021FI2.API.TypeChambre;
import fr.iutfbleau.projetIHM2021FI2.MNP.ClientNP;
import fr.iutfbleau.projetIHM2021FI2.MNP.PrereservationNP;

public class SearchListener implements ActionListener {

    private JTextField prenomField;
    private JTextField nomField;
    private JTextField refField;
    private DefaultTableModel model;
    private Prereservation preRes;
    private Set<Prereservation> setPreres;
    private Iterator<Prereservation> preresItr;
    // private Set<Prereservation> preResList;

    // String prenom;
    // String nom;
    // String Reference;

    public SearchListener(JTextField prenomField, JTextField nomField, JTextField refField, DefaultTableModel model) {
        this.prenomField = prenomField;
        this.nomField = nomField;
        this.refField = refField;
        this.model = model;

        setPreres = new HashSet<Prereservation>();

        setPreres.add(new PrereservationNP("4751-3708-LRMF", LocalDate.of(2018, 01, 05), 1, TypeChambre.UNLD, new ClientNP(1, "Marine", "Carpentier")));
        setPreres.add(new PrereservationNP("2436-8461-NXLL", LocalDate.of(2018, 04, 10), 3, TypeChambre.UNLD, new ClientNP(1, "Marine", "Carpentier")));
        setPreres.add(new PrereservationNP("0564-7481-EKLG", LocalDate.of(2018, 04, 10), 7, TypeChambre.UNLD, new ClientNP(1, "Charles", "Leclerc")));
        setPreres.add(new PrereservationNP("4561-7524-RTID", LocalDate.of(2018, 04, 11), 7, TypeChambre.UNLD, new ClientNP(1, "Fernando", "Alonso")));

        preresItr = setPreres.iterator();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // System.out.println("Recherche...");
        // System.out.println("Prénom = " + prenomField.getText() + ", Nom = " + nomField.getText() + ", Numéro de Réservation = " + refField.getText());
        
        clearTable();
        
        model.addRow(new Object[] {"4751-3708-LRMF", "2018-01-05", 1, "UNLD", 1, "Marine", "Carpentier"});
        model.addRow(new Object[] {"2436-3909-NXLL", "2018-01-07", 1, "UNLS", 1, "Marine", "Carpentier"});
        model.addRow(new Object[] {"5432-7853-LRMF", "2018-01-05", 1, "UNLD", 1, "Charles", "Test"});
        model.addRow(new Object[] {"1234-7537-SUCE", "2018-01-07", 1, "UNLS", 1, "Jean", "Jacques"});

        tableConstructor();
    }

    // Remove all row already present
    void clearTable() {
        //model.getDataVector().removeAllElements();
		int rowCount = model.getRowCount();
		for (int i = rowCount - 1; i >= 0; i--) {
			model.removeRow(i);
		}
    }

    // A constructor for a JTable row
    public Object[] rowConstructor(Prereservation preres) {
        String[] row = new String[] {preres.getReference(), preres.getDateDebut().toString(), String.valueOf(preres.getJours()),
                                     preres.getTypeChambre().name(), String.valueOf(preres.getClient().getId()),
                                     String.valueOf(preres.getClient().getPrenom()), String.valueOf(preres.getClient().getNom())};
        
        return row;
    }

    public void tableConstructor() {
        while (preresItr.hasNext()) {
            model.addRow(rowConstructor(preresItr.next()));
        }
        System.out.println("Après la boucle while");
    }

    // public void Requete() {
    //     preRes = new PreReservationBD();
        
    //     preResList = preRes.getPrereservations(nomField.getText(), prenomField.getText());
    //     if (preResList.isEmpty()) {
    //         preResList = preRes.getPrereservation(refField.getText());
    //     }
    // }

    // public void setPrenom(String prenom) {
    //     this.prenom = prenom;
    // }

    // public void setNom(String nom) {
    //     this.nom = nom;
    // }

    // public void setReference(String Reference) {
    //     this.Reference = Reference;
    // }
    
}
