package fr.iutfbleau.projetIHM2021FI2.Controleur;

import java.util.HashSet;
import java.util.Set;

import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import fr.iutfbleau.projetIHM2021FI2.Vue.BottomContainer;

public class RowListener implements ListSelectionListener {

    private JTable table;
    private BottomContainer botContainer;

    private int selectedRow;
    
    private Set<String> rowContent;

    public RowListener(JTable table, BottomContainer botContainer) {
        this.table = table;
        this.botContainer = botContainer;

        this.rowContent = new HashSet<>();
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        System.out.println("Dans bloc listener ligne sélectionné");
        System.out.println(this.table.getSelectedColumnCount());

        selectedRow = this.table.getSelectedRow();

        for (int i = 0; i < 7; i++) {
            rowContent.add((String) table.getValueAt(selectedRow, i));
            System.out.println(table.getValueAt(selectedRow, i));
        }

        botContainer.setRowContent(rowContent);
        botContainer.addNewDataAndRefresh();
    }
    
}
