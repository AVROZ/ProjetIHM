package fr.iutfbleau.projetIHM2021FI2.Controleur;

import java.awt.event.*;
import javax.swing.JTextField;

public class NomListener implements FocusListener {

    @Override
    public void focusGained(FocusEvent e) {
        JTextField nom = (JTextField) e.getSource();
        if (nom.getText().equals("Nom")) {
            nom.setText("");
        }
    }

    @Override
    public void focusLost(FocusEvent e) {
        JTextField nom = (JTextField) e.getSource();
        if (nom.getText().equals("")) {
            nom.setText("Nom");
        }
    }

}
