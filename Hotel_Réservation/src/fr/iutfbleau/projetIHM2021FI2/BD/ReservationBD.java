package fr.iutfbleau.projetIHM2021FI2.BD;

import java.sql.*;
import org.mariadb.jdbc.*;
import java.util.*;
import java.time.LocalDate;
import fr.iutfbleau.projetIHM2021FI2.API.*;
import fr.iutfbleau.projetIHM2021FI2.MNP.*;

public class ReservationBD {
	Connection bd;

	public ReservationBD() {
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			try {
				this.bd = DriverManager.getConnection(
					"jdbc:mariadb://dwarves.iut-fbleau.fr/ungar",
					"ungar", "mdpBDDungar2021");
				System.out.println("Connecté.");
			} catch(SQLException e) {
				System.err.println(e + "\nERROR : Connection failed");
			}
		} catch(ClassNotFoundException e2) {
			System.err.println("ERROR - Class not found");
		}			
	}

	public void closeConnection() {
		try {
			this.bd.close();
			System.out.println("Connection closed");
		} catch(SQLException e) {
			System.err.println(e + "\nConnection cannot be closed");
		}
	}

	public int createReservation(String reference, LocalDate dateDebut, int jours, Chambre chambre, Client client) {
		try {
			if(!clientInDB(client.getId())) {
				PreparedStatement ps = this.bd.prepareStatement("INSERT INTO Client (id, prenom, nom) VALUES ("+client.getId()+", '"+client.getPrenom()+"', '"+client.getNom()+"');");
				ps.executeUpdate();
				ps.close();
			}
		} catch(SQLException e) {
			System.err.println(e + "\nErreur Client");
		}
		try {
			if(!reservationInDB(reference)) {
				PreparedStatement ps = this.bd.prepareStatement("INSERT INTO Reservation (id, reference, debut, nuits, chambre, client) VALUES (NULL, '"+reference+"', '"+dateDebut+"', "+jours+", "+chambre.getNumero()+", "+client.getId()+");");
				ps.executeUpdate();
				ps.close();
			}
		} catch(SQLException e) {
			System.err.println(e);
			return 0;
		}
		return 1;
	}

	private boolean clientInDB(int id) {
		try {
			PreparedStatement ps = this.bd.prepareStatement("SELECT id FROM Client WHERE id="+id+";");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				System.out.println("Client déjà dans la BD");
				return true;
			}
			ps.close();
		} catch(SQLException e) {
			System.err.println(e + "Erreur ClientInDB");
		}
		return false;
	}

	private boolean reservationInDB(String reference) {
		try {
			PreparedStatement ps = this.bd.prepareStatement("SELECT reference FROM Reservation WHERE reference='"+reference+"';");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				System.out.println("Réservation déjà dans la BD");
				return true;
			}
			ps.close();
		} catch(SQLException e) {
			System.err.println(e + "Erreur reservationInDB");
		}
		return false;
	}

	/*private static void printPreresa(Prereservation pre) {
		System.out.println("\nReference : " + pre.getReference());
		System.out.println("Date : " + pre.getDateDebut());
		System.out.println("Jours : " + pre.getJours());
		System.out.println("Type Chambre : " + pre.getTypeChambre());
		System.out.println("Client : " + pre.getClient());
		System.out.println("ID : " + pre.getClient().getId());
		System.out.println("Prenom : " + pre.getClient().getPrenom());
		System.out.println("Nom : " + pre.getClient().getNom());
	}*/
}