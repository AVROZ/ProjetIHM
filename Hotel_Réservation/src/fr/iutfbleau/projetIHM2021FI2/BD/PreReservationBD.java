package fr.iutfbleau.projetIHM2021FI2.BD;

import java.sql.*;
import org.mariadb.jdbc.*;
import java.util.*;
import fr.iutfbleau.projetIHM2021FI2.API.*;
import fr.iutfbleau.projetIHM2021FI2.MNP.*;

public class PreReservationBD {
	Connection bd;
	Client clientDB;

	public PreReservationBD() {
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			try {
				this.bd = DriverManager.getConnection(
					"jdbc:mariadb://dwarves.iut-fbleau.fr/projetihm",
					"projetihm", "mhitejorp");
				System.out.println("Connecté.");
			} catch(SQLException e) {
				System.err.println(e + "\nERROR : Connection failed");
			}
		} catch(ClassNotFoundException e2) {
			System.err.println("ERROR - Class not found");
		}
		this.clientDB = new ClientNP(0, "null", "null");		
	}

	public void closeConnection() {
		try {
			this.bd.close();
			System.out.println("Connection closed");
		} catch(SQLException e) {
			System.err.println(e + "\nConnection cannot be closed");
		}
	}

	public Set<Prereservation> getPrereservations(String n, String p) {
		Set<Prereservation> prereservations = new HashSet<Prereservation>();
		try {
			PreparedStatement ps = this.bd.prepareStatement("SELECT reference, debut, categorie, nuits, C.id, prenom, nom FROM Reservation R, Client C WHERE R.client=C.id AND C.nom='"+n+"' AND C.prenom='"+p+"';");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				if(this.clientDB.getId() != rs.getInt(5)) {
					System.out.println("Création client set");
					this.clientDB = new ClientNP(rs.getInt(5), rs.getString(6), rs.getString(7));
				}
				prereservations.add(new PrereservationNP(rs.getString(1), rs.getDate(2).toLocalDate(), rs.getInt(4), this.getTypeChambreById(rs.getInt(3)),  this.clientDB));
			}
			rs.close();
			ps.close();
		} catch(SQLException e) {
			System.err.println(e);
			return null;
		}
		printSet(prereservations);
		return prereservations;
	}

	public Prereservation getPrereservation(String reference) {
		Prereservation prereservation = null;
		try {
			PreparedStatement ps = this.bd.prepareStatement("SELECT reference, debut, categorie, nuits, C.id, prenom, nom FROM Reservation R, Client C WHERE R.client=C.id AND R.reference='"+reference+"';");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				if(this.clientDB.getId() != rs.getInt(5)) {
					System.out.println("Création client");
					this.clientDB = new ClientNP(rs.getInt(5), rs.getString(6), rs.getString(7));
				}
				prereservation = new PrereservationNP(rs.getString(1), rs.getDate(2).toLocalDate(), rs.getInt(4), this.getTypeChambreById(rs.getInt(3)), this.clientDB);
			}
			rs.close();
			ps.close();
		} catch(SQLException e) {
			System.err.println(e);
			return null;
		}
		System.out.println(prereservation);
		return prereservation;
	}

	private TypeChambre getTypeChambreById(int nb) {
		switch(nb) {
			case 1:  return TypeChambre.UNLS;
			case 2:  return TypeChambre.UNLD;
			case 3:  return TypeChambre.DEUXLS;
			default: return null;
		}
	}

	private static void printSet(Set<Prereservation> prereservations) {
		for(Prereservation pre:prereservations) {
			System.out.println(pre);
		}
	}

	/*private static void printPreresa(Prereservation pre) {
		System.out.println("\nReference : " + pre.getReference());
		System.out.println("Date : " + pre.getDateDebut());
		System.out.println("Jours : " + pre.getJours());
		System.out.println("Type Chambre : " + pre.getTypeChambre());
		System.out.println("Client : " + pre.getClient());
		System.out.println("ID : " + pre.getClient().getId());
		System.out.println("Prenom : " + pre.getClient().getPrenom());
		System.out.println("Nom : " + pre.getClient().getNom());
	}*/
}